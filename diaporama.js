var min0 = 0;
var min1 = 1;
var min2 = 2;
var min3 = 3;
var min4 = 4;
var min5 = 5;
var min6 = 6;
var min7 = 7;

//Start - Boucle img
var images = new Array();
images.push("img/1.jpg");
images.push("img/2.jpg");
images.push("img/3.jpg");
images.push("img/4.jpg");
images.push("img/5.jpg");
images.push("img/6.jpg");
images.push("img/7.jpg");

var i = 0;
var Timeout;

function start() {
    document.getElementById("img").src = images[i];

    if (i < images.length - 1) {
        i++;
    }
    else {
        i = 0;
    }
    Timeout = setTimeout(start, 1500)
}

// Stop
function stop() {
    clearTimeout(Timeout);
}

//premier
function premiere() {
    document.getElementById("img").src = images[0];
}

//derniere
function derniere() {
    document.getElementById("img").src = images[images.length - 1];
}

//suivant
function suivant() {

    i++;
    if (i > images.length - 1) {
        i = 0
    }
    document.getElementById("img").src = images[i];
}

//precedent
function precedent() {
    i--;
    if (i < 0) {
        i = images.length - 1;
    }
    document.getElementById("img").src = images[i];
}

//aleatoire
function aleatoire() {
    var rand = Math.floor(Math.random() * images.length);
    document.getElementById("img").src = images[rand];
    console.log(rand);
}

//function galerie click
function galerie0() {
    document.getElementById("img").src = images[0];
}
function galerie1() {
    document.getElementById("img").src = images[1];
}
function galerie2() {
    document.getElementById("img").src = images[2];
}
function galerie3() {
    document.getElementById("img").src = images[3];
}
function galerie4() {
    document.getElementById("img").src = images[4];
}
function galerie5() {
    document.getElementById("img").src = images[5];
}
function galerie6() {
    document.getElementById("img").src = images[6];
}
function galerie7() {
    document.getElementById("img").src = images[7];
}

//if()
//images.src 

//galerie  precedent
function miniaturePrecedente() {

    var miniature0 = document.getElementById("miniature0")
    var miniature1 = document.getElementById("miniature1")
    var miniature2 = document.getElementById("miniature2")
    var miniature3 = document.getElementById("miniature3")

    min0--;
    min1--;
    min2--;
    min3--;

    if (min0 < 0) {
        min0 = images.length - 1;
    }
    if (min1 < 0) {
        min1 = images.length - 1;
    }
    if (min2 < 0) {
        min2 = images.length - 1;
    }
    if (min3 < 0) {
        min3 = images.length - 1;
    }

    miniature0.src = images[min0];
    miniature1.src = images[min1];
    miniature2.src = images[min2];
    miniature3.src = images[min3];
}

//galerie suivant
function miniatureSuivante() {

    var miniature0 = document.getElementById("miniature0")
    var miniature1 = document.getElementById("miniature1")
    var miniature2 = document.getElementById("miniature2")
    var miniature3 = document.getElementById("miniature3")

    min0++;
    min1++;
    min2++;
    min3++;

    if (min0 > images.length-1) {
        min0 = 0 ;
    }
    if (min1 > images.length-1) {
        min1 = 0 ;
    }
    if (min2 > images.length-1) {
        min2 = 0 ;
    }
    if (min3 > images.length-1) {
        min3 = 0 ;
    }
    miniature0.src = images[min0];
    miniature1.src = images[min1];
    miniature2.src = images[min2];
    miniature3.src = images[min3];
}
